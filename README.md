# Mixedmethodstools

A collection of tools for mixed-methods research

## Background

I'm taking Mixed Methods from Yuan Hsaio this quarter (Win 2021). This project collects various resources and tidbits to support mixed methods research.

## Contents

* Typology Table Template -- I have a hard time composing nested tables on the fly in LaTeX. So I used https://www.tablesgenerator.com/ to make one like what Creamer (2018) recommends. See creamerTemplate.png for a sample of what it looks like before being filled out.


Creamer, Elizabeth G. (2018) *An Introduction to Fully Integrated Mixed Methods Research*. SAGE.
